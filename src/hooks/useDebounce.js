import PropTypes from "prop-types";
import { useEffect, useState } from "react";

const useDebounce = (searchValue, delay) => {
  const [debouncedValue, setDebouncedValue] = useState(searchValue);

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(searchValue);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [searchValue, delay]);

  return debouncedValue;
};

export default useDebounce;

useDebounce.propTypes = {
  searchValue: PropTypes.string,
  delay: PropTypes.number.isRequired,
};

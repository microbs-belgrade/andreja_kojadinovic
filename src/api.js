import axios from "axios";
// Constance
import { ITUNES_URL } from "./utilities/constants";

export const getAlbums = async (searchValue) => {
  let allAlbums = await axios
    .get(`${ITUNES_URL}=${searchValue}`, {
      "Content-Type": "application/json",
    })
    .then((res) => {
      if (res?.data?.results) {
        return res?.data?.results;
      }
    })
    .catch((error) => {
      console.log("error", error);
    });

  return allAlbums;
};

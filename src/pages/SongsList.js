import { useState, useEffect } from "react";
// Hooks
import useDebounce from "../hooks/useDebounce";
// Components
import Song from "../components/Song";
// Helpers
import { getSortedList } from "../utilities/helpers";
// API
import { getAlbums } from "../api";
// Constance
import {
  DEFAULT_LIST,
  INTERVAL_TIME,
  DELAY_TIME,
} from "../utilities/constants";
// Styles
import "../styles/SongsList.css";

const SongsList = () => {
  const [songs, setSongs] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [response, setResponse] = useState([]);
  const debouncedSearch = useDebounce(searchTerm, DELAY_TIME);

  useEffect(() => {
    setSongs(DEFAULT_LIST);
    setResponse(DEFAULT_LIST);
  }, []);

  useEffect(() => {
    if (songs?.length > 0) {
      const interval = setInterval(() => {
        setSongs((prevData) => {
          const allAlbums = [...prevData];
          const item = allAlbums.shift();

          const isNewAlbum = response.find(
            (responseSong) =>
              responseSong.collectionName === item.collectionName
          );

          isNewAlbum && allAlbums.push(item);

          return allAlbums;
        });
      }, INTERVAL_TIME);
      return () => clearInterval(interval);
    }
  }, [songs, response, searchTerm]);

  useEffect(() => {
    const getSearchAlbums = async () => {
      if (debouncedSearch) {
        const allAlbums = await getAlbums(debouncedSearch);
        if (allAlbums?.length > 0) {
          const sortedAlbums = getSortedList(allAlbums);
          const firstFiveAlbums = sortedAlbums.splice(0, 5);

          setSongs((prevData) => [...prevData, ...firstFiveAlbums]);
          setResponse(firstFiveAlbums);
        }
      }
    };

    getSearchAlbums();
  }, [debouncedSearch]);

  const handleChangeSearch = (event) => {
    setSearchTerm(event.target.value);
  };

  const renderSongs = (song, index) =>
    index < 5 && <Song key={index} collectionName={song.collectionName} />;

  return (
    <div className="list-wrapper">
      <input
        className="search-field"
        placeholder="Search songs"
        value={searchTerm}
        onChange={handleChangeSearch}
      />
      <div className="list">{songs?.length > 0 && songs.map(renderSongs)}</div>
    </div>
  );
};

export default SongsList;

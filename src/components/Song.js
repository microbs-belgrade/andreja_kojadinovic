import PropTypes from "prop-types";
// Styles
import "../styles/Song.css";

const Song = ({collectionName}) => <div className="song">{collectionName}</div>;

export default Song;

Song.propTypes = {
  collectionName: PropTypes.string.isRequired,
};

// Pages
import SongsList from "./pages/SongsList";
// Styles
import "./App.css";

const App = () => {
  return (
    <div className="app">
        <SongsList />
    </div>
  );
}

export default App;

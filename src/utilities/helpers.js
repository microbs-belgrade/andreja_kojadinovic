export const getSortedList = (list) =>
  list?.sort((a, b) => a?.collectionName?.localeCompare(b?.collectionName));

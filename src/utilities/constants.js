export const ITUNES_URL = "https://itunes.apple.com/search?term";
export const DEFAULT_LIST = [
  {
    collectionName: "A",
  },
  {
    collectionName: "B",
  },
  {
    collectionName: "C",
  },
  {
    collectionName: "D",
  },
  {
    collectionName: "E",
  },
];
export const INTERVAL_TIME = 1000;
export const DELAY_TIME = 500;
